;;; cjkv.el ---  -*- lexical-binding: t; -*-

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190411
;; Updated: 20241106
;; Version: 0.0
;; Keywords: cjkv

;;; Commentary:

;;; Change Log:


;;; Code:


(provide 'cjkv)


(require '假名)


(defvar cjkv/regex-charClass-expansion-alist
  `((":平仮名:" . ,假名/平假名--one-of-each)
    )
  "Associate extended cjkv character classes
with equivalent standard character classes."
     )

(defun cjkv/regex-expand (cjkv-regex)
    "Expand string CJKV-REGEX to standard emacs regex"
    (with-temp-buffer
      (insert cjkv-regex)
      (goto-char (point-min))
      (dolist (elem cjkv/regex-charClass-expansion-alist)
        (let ((cjkv     (car elem))
              (standard (cdr elem))
              )
          (while (search-forward (format "[%s]" cjkv) nil t)
            (replace-match standard nil t)
            )))
      (buffer-string)
      ))



(provide 'cjkv)

;;; cjkv.el ends here
