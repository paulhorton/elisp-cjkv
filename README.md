# A few utilities for working with languages using Chinese characters in the past or present.

## AUTHOR

Paul Horton.  ©2019.


## 漢字.el

Contains functions working on Chinese characters (as opposed to words, etc.)

Provides:
- command: hz/num-strokes 漢字/筆畫數
  (漢字/筆畫數 ?漢) --> (13)
  or run as command with cursor on top of character of interest.
- variable: hz/char-variants-JPTW-caseTable 漢字/異字體-正日caseTable 
  Allows for loose hanzi matching (e.g. 巢 with 巣).
                                           (string-match "巢" "巣")  --> nil
  (with-case-table 漢字/異字體-正日caseTable (string-match "巢" "巣")) --> 0

  To use interactively setup with command: (set-case-table 漢字/異字體-正日caseTable)
  and make sure case-fold-search is non-nil



Try out (search-forward "単位") or (upcase-region) on this text:

        單數	dan1shu4	odd number
        單位	dan1wei4	a unit

