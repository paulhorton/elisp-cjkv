;;; cjkv-utils.el --- 

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20191116
;; Updated: 20191116
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:

(provide 'cjkv-utils)


(defmacro cjkv/-to-string (char-or-string)
  "Convert char-to-string to a string."
  (when (characterp char-or-string)
    (setq char-or-string (string char-or-string))))


(defun cjkv/-aset_add-to-list (array idx newElt)
  "Push newElt onto list found at (aref ARRAY IDX), creating the list if needed.
Nothing done if newElt is already in the list."
  (let ((list-ref (aref array idx)))
    (if list-ref
        (unless (assoc newElt list-ref)
          (push list-ref newElt))
      (aset array idx (list newElt)))))




;;; cjkv-utils.el ends here

