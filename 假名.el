;;; 假名.el --- Functions related to Japanese hiragana and katakana characters.  -*- lexical-binding: t; -*-
;;;

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190309
;; Updated: 20241106
;; Version: 0.0
;; Keywords: 假名, かな, kana

;;; Commentary:

;; Ideas:  Define case-table for 濁音,半濁音 insensitive string matching, e.g. let 'がまえ' match 'かまえ'.

;;; Change Log:

;;; Code:


(provide '假名)
(require 'tr)


(defvar 假名/平假名--one-of-each
  "あぁいぃうゔぅえぇおぉかがきぎくぐけげゖヶこごさざしじすずせぜそぞただちぢつづってでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもやゃゆゅよょらりるれろわゐゑをん゙゚゛゜ゝゞゟー"
    "String containing one instance of each possible hiragana chararacter")

(defvar 假名/片假名--one-of-each
  "アァイィウヴゥエェオォカガキギクグケゲゖヶコゴサザシジスズセゼソゾタダチヂツヅッテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモヤャユュヨョラリルレロワヰヱヲン゙゚゛゜ゝゞゟー"
    "String containing one instance of each possible katakana chararacter")


(defvar 假名/平假名-片假名CSTAB
  (tr/caseTable/2seqs 假名/平假名--one-of-each 假名/片假名--one-of-each)
  "Case table mapping hiragana ⟷ katakana")


(defconst 假名/平假名:片假名:半角
  '((?あ ?ア "ｱ")  (?ぁ ?ァ "ｧ")
    (?い ?イ "ｲ")  (?ぃ ?ィ "ｨ")
    (?う ?ウ "ｳ")  (?ぅ ?ゥ "ｩ")  (?ゔ ?ヴ "ｳﾞ")
    (?え ?エ "ｴ")  (?ぇ ?ェ "ｪ")
    (?お ?オ "ｵ")  (?ぉ ?ォ "ｫ")
    (?か ?カ "ｶ")  (nil ?ヵ nil) (?が ?ガ "ｶﾞ")
    (?き ?キ "ｷ")                (?ぎ ?ギ "ｷﾞ")
    (?く ?ク "ｸ")                (?ぐ ?グ "ｸﾞ")
    (?け ?ケ "ｹ")  (nil ?ヶ nil) (?げ ?ゲ "ｹﾞ")
    (?こ ?コ "ｺ")                (?ご ?ゴ "ｺﾞ")
    (?さ ?サ "ｻ")                (?ざ ?ザ "ｻﾞ")
    (?し ?シ "ｼ")  (nil ?ㇱ nil) (?じ ?ジ "ｼﾞ")
    (?す ?ス "ｽ")  (nil ?ㇲ nil) (?ず ?ズ "ｽﾞ")
    (?せ ?セ "ｾ")                (?ぜ ?ゼ "ｾﾞ")                  (nil "セ゚" nil)
    (?そ ?ソ "ｿ")                (?ぞ ?ゾ "ｿﾞ")
    (?た ?タ "ﾀ")                (?だ ?ダ "ﾀﾞ")
    (?ち ?チ "ﾁ")                (?ぢ ?ヂ "ﾁﾞ")
    (?つ ?ツ "ﾂ")  (?っ ?ッ "ｯ")  (?づ ?ヅ "ﾂﾞ")                  (nil "ツ゚" nil)
    (?て ?テ "ﾃ")               (?で ?デ "ﾃﾞ")
    (?と ?ト "ﾄ")  (nil ?ㇳ nil)  (?ど ?ド "ﾄﾞ")                  (nil "ト゚" nil)
    (?な ?ナ "ﾅ")
    (?に ?ニ "ﾆ")
    (?ぬ ?ヌ "ﾇ")  (nil ?ㇴ nil)
    (?ね ?ネ "ﾈ")
    (?の ?ノ "ﾉ")
    (?は ?ハ "ﾊ")  (nil ?ㇵ nil) (?ば ?バ "ﾊﾞ")    (?ぱ ?パ "ﾊﾟ")
    (?ひ ?ヒ "ﾋ")  (nil ?ㇶ nil) (?び ?ビ "ﾋﾞ")    (?ぴ ?ピ "ﾋﾟ")
    (?ふ ?フ "ﾌ")  (nil ?ㇷ nil) (?ぶ ?ブ "ﾌﾞ")    (?ぷ ?プ "ﾌﾟ")  (nil "ㇷ゚" nil)
    (?へ ?ヘ "ﾍ")  (nil ?ㇸ nil) (?べ ?ベ "ﾍﾞ")    (?ぺ ?ペ "ﾍﾟ")
    (?ほ ?ホ "ﾎ")  (nil ?ㇹ nil) (?ぼ ?ボ "ﾎﾞ")    (?ぽ ?ポ "ﾎﾟ")
    (?ま ?マ "ﾏ")
    (?み ?ミ "ﾐ")
    (?む ?ム "ﾑ")  (nil ?ㇺ nil)
    (?め ?メ "ﾒ")
    (?も ?モ "ﾓ")
    (?や ?ヤ "ﾔ")  (?ゃ ?ャ "ｬ")
    (?ゆ ?ユ "ﾕ")  (?ゅ ?ュ "ｭ")
    (?よ ?ヨ "ﾖ")  (?ょ ?ョ "ｮ")
    (?ら ?ラ "ﾗ")  (nil ?ㇻ nil)
    (?り ?リ "ﾘ")  (nil ?ㇼ nil)
    (?る ?ル "ﾙ")  (nil ?ㇽ nil)
    (?れ ?レ "ﾚ")  (nil ?ㇾ nil)
    (?ろ ?ロ "ﾛ")  (nil ?ㇿ nil)
    (?わ ?ワ "ﾜ")  (?ゎ ?ヮ "ﾜ")
    (?ゐ "ヰ" nil)
    (?ゑ "ヱ" nil)
    (?を ?ヲ "ｦ")  (?𛅒 ?𛅦 nil)
    (?ん ?ン "ﾝ")
    (?ゝ ?ヽ nil)                (?ゞ ?ヾ nil)
    ))



(defvar 假名/半角←假名CHTAB
  (make-char-table 'init)
  "Char table mapping kana to their hankaku equivalents")

(cl-loop
 for (平假名 片假名 半角) in 假名/平假名:片假名:半角
 if 半角
 do (progn (aset 假名/半角←假名CHTAB 平假名 半角)
           (aset 假名/半角←假名CHTAB 片假名 半角)))


(defun 假名/半角-insert (S)
  "Do (insert R), where R is S converted to hankaku."
  (cl-loop
   for c across S
   do  (insert (or  (aref 假名/半角←假名CHTAB c)  c))
   ))


(defun 假名/平假名CHAR? (c)
  "Return non-nil if C is a hiragana character."
  (cl-find c 假名/平假名--one-of-each)
  )

(defun 假名/片假名CHAR? (c)
  "Return non-nil if C is a katakana character."
  (cl-find c 假名/片假名--one-of-each)
  )

(defun 假名/假名CHAR? (c)
  "Return non-nil iff character C is a kana character"
  (or (假名/平假名CHAR? c)
      (假名/片假名CHAR? c)
      ))

(defun 假名/假名any? (s)
  "Return t if any characters of S are kana (hiragana or katakana)"
  (cjkv/-to-string s)
  (seq-some #'假名/假名CHAR? s)
  )

(defun 假名/片假名all? (s)
  "Return t if S consists entirely of Katakana"
  (cjkv/-to-string s)
  (seq-every-p #'假名/片假名CHAR? s))

(defun 假名/平假名all? (s)
  "Return t if S consists entirely of Hiragana"
  (cjkv/-to-string s)
  (seq-every-p #'假名/平假名CHAR? s))


(defun 假名/平假名←片假名 (s)
  "Return copy of S
with (full-width) katakana converted to their corresponding katakana."
  (with-case-table 假名/平假名-片假名CSTAB
    (upcase s)
    ))

(defun 假名/片假名←平假名 (s)
  "Return copy of S
with (full-width) katakana converted to their corresponding hiragana."
  (with-case-table 假名/平假名-片假名CSTAB
    (downcase s)
    ))

(defun 假名/平假名→片假名 (s)
  "Convert (full-width) hiragana in string S to corresponding katakana"
  (tr/dwcase 假名/平假名-片假名CSTAB s))

(defun 假名/片假名→平假名 (s)
  "Convert (full-width) katakana in string S to corresponding hiragana"
  (tr/upcase 假名/平假名-片假名CSTAB s))


(defun 假名/筆畫數 (c)
  "If C is a katakan character,
return the nuumber of strokes;
otherwise just return nil."
  ;; Currently just a stub
  (when (eq c ?ヒ) 2)
  )


(provide '假名)

;;; 假名.el ends here
